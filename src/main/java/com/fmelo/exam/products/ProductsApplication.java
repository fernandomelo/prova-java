package com.fmelo.exam.products;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class ProductsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductsApplication.class, args);
	}
	
	@RestController
	public static class HomeController {

		@GetMapping("/")
		public String get() {
			return "FC EXAM API - Fernando Melo";
		}
	}

}
