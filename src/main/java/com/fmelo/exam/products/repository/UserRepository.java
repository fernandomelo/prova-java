package com.fmelo.exam.products.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.fmelo.exam.products.model.User;

public interface UserRepository extends MongoRepository<User, String> {

}
