package com.fmelo.exam.products.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.fmelo.exam.products.model.Product;

public interface ProductRepository extends MongoRepository<Product, String> {

}