package com.fmelo.exam.products.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.fmelo.exam.products.model.Link;

public interface LinkRepository extends MongoRepository<Link, String> {

	@Query("{ 'user.id' : ?0, 'product.id': ?1, active: true }")
	Link findActiveByUserAndProduct(String userId, String productId);

	@Query("{ 'product.id': ?0, active: true }")
	List<Link> findActiveByProductId(String productId);

	@Query("{ 'user.id': ?0 }")
	List<Link> findAllByUserId(String userId);

	@Query("{ active: true }")
	List<Link> findActive();

}
