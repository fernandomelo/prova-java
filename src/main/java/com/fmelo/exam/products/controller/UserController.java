package com.fmelo.exam.products.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fmelo.exam.products.model.User;
import com.fmelo.exam.products.repository.UserRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "User", description = "This controller enables the CRUD operations for the entity User.")
@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserRepository userRepository;

	@ApiOperation(value = "List all users")
	@GetMapping("/")
	public List<User> getAll() {
		return userRepository.findAll();
	}

	@ApiOperation(value = "Get a specific user")
	@GetMapping("/{id}")
	public ResponseEntity<User> getById(@ApiParam(value = "UserId of the user wanted") @PathVariable("id") String id) {
		Optional<User> user = userRepository.findById(id);
		
		if (!user.isPresent()) {
			return new ResponseEntity<User>(null, null, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<User>(user.get(), null, HttpStatus.OK);
	}

	@ApiOperation(value = "Create user")
	@PostMapping("/")
	public ResponseEntity<User> create(@ApiParam(value = "User body (JSON)") @RequestBody User user) {
		return new ResponseEntity<User>(userRepository.save(user), null, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Update user")
	@PostMapping("/{id}")
	public ResponseEntity<User> update(@ApiParam(value = "User body (JSON) after the desired changes were applied") @RequestBody User user,
			@ApiParam(value = "UserId of the user you want to update") @PathVariable("id") String id) {
		user.setId(id);
		return new ResponseEntity<User>(userRepository.save(user), null, HttpStatus.OK);
	}

	@ApiOperation(value = "Delete user")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@ApiParam(value = "UserId of the user you want to delete") @PathVariable("id") String id) {
		Optional<User> user = userRepository.findById(id);
		
		if (!user.isPresent()) {
			return new ResponseEntity<Void>(null, null, HttpStatus.NOT_FOUND);
		}
		
		userRepository.deleteById(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
}
