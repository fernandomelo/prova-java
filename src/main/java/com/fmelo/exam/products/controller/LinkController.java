package com.fmelo.exam.products.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fmelo.exam.products.model.Link;
import com.fmelo.exam.products.model.Product;
import com.fmelo.exam.products.model.User;
import com.fmelo.exam.products.repository.LinkRepository;
import com.fmelo.exam.products.repository.ProductRepository;
import com.fmelo.exam.products.repository.UserRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "Link", description = "This controller is meant to manage the relationship (Link) between User and Product.")
@RestController
@RequestMapping("/link")
public class LinkController {

	@Autowired
	private LinkRepository linkRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ProductRepository productRepository;

	@ApiOperation(value = "List all links")
	@GetMapping("/")
	public List<Link> getAll() {
		return linkRepository.findAll();
	}

	@ApiOperation(value = "It creates the link between the informed User and Product. It also inactivates a previous equivalent link if found.")
	@PostMapping("/user/{userId}/product/{productId}")
	public ResponseEntity<Link> link(
			@ApiParam(value = "UserId of the user you want to link") @PathVariable("userId") String userId,
			@ApiParam(value = "ProductId of the product you want to link") @PathVariable("productId") String productId) {
		Link active = linkRepository.findActiveByUserAndProduct(userId, productId);

		if (active == null) {
			//verificação para salvar somente se usuário e produto existirem
			Optional<User> possibleUser = userRepository.findById(userId);
			Optional<Product> possibleProduct = productRepository.findById(productId);
			
			if (!possibleUser.isPresent() || !possibleProduct.isPresent())
				return new ResponseEntity<Link>(null, null, HttpStatus.NOT_FOUND);
		} else {
			//se existe um link ativo correspondente, nao precisa validar usuário e produto
			active.setActive(false);
			linkRepository.save(active);
		}

		Link newLink = new Link();
		newLink.setActive(true);
		newLink.setDate(LocalDateTime.now());
		newLink.setProduct(new Product(productId));
		newLink.setUser(new User(userId));

		return new ResponseEntity<Link>(linkRepository.save(newLink), null, HttpStatus.CREATED);
	}

	@ApiOperation(value = "List all users that are linked to the supplied product.")
	@GetMapping("/product/{productId}")
	public List<Link> getUsersByProduct(
			@ApiParam(value = "ProductId of the product you want to get the linked users") @PathVariable("productId") String productId) {
		
		List<Link> links = linkRepository.findActiveByProductId(productId);;
		
		fillLinksWithUserData(links);

		return links;
	}
	
	@ApiOperation(value = "Get all active links of all products.")
	@GetMapping("/product/")
	public List<Link> getActiveUsers() {
		
		List<Link> links = linkRepository.findActive();
		
		fillLinksWithUserData(links);
		
		fillLinksWithProductData(links);

		return links;
	}

	private void fillLinksWithUserData(List<Link> links) {
		List<User> users = (List<User>) userRepository
				.findAllById(links.stream().map(l -> l.getUser().getId()).collect(Collectors.toList()));
		Map<String, User> userMap = users.stream().collect(Collectors.toMap(User::getId, u -> u));

		links.forEach(l -> l.setUser(userMap.get(l.getUser().getId())));
	}

	@ApiOperation(value = "List all products (active and inactive) that are linked to the supplied user")
	@GetMapping("/user/{userId}")
	public List<Link> getProductsByUser(
			@ApiParam(value = "UserId of the user you want to get the linked products") @PathVariable("userId") String userId) {
		List<Link> links = linkRepository.findAllByUserId(userId);

		fillLinksWithProductData(links);

		return links;
	}

	private void fillLinksWithProductData(List<Link> links) {
		List<Product> products = (List<Product>) productRepository
				.findAllById(links.stream().map(l -> l.getProduct().getId()).collect(Collectors.toList()));
		Map<String, Product> productMap = products.stream().collect(Collectors.toMap(Product::getId, p -> p));

		links.forEach(l -> l.setProduct(productMap.get(l.getProduct().getId())));
	}

}
