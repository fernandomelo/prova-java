package com.fmelo.exam.products.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fmelo.exam.products.model.Product;
import com.fmelo.exam.products.repository.ProductRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "Product", description = "Only useful to list all available products.")
@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private ProductRepository productRepository;

	@ApiOperation(value = "List all products")
	@GetMapping("/")
	public List<Product> getAll() {
		return productRepository.findAll();
	}

}
