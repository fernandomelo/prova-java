package com.fmelo.exam.products.controller;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.fmelo.exam.products.model.Link;
import com.fmelo.exam.products.model.Product;
import com.fmelo.exam.products.model.User;
import com.fmelo.exam.products.repository.LinkRepository;
import com.fmelo.exam.products.repository.ProductRepository;
import com.fmelo.exam.products.repository.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class LinkControllerTest {
	
	@Mock
	private LinkRepository linkRepository;

	@Mock
	private UserRepository userRepository;

	@Mock
	private ProductRepository productRepository;

	@InjectMocks
	public LinkController controller;
	
	@Test
	public void shouldCreateLink() throws Exception {
		String productId = "444444";
		String userId = "222222";
		String insertedLinkId = "123456789";
		Link inserted = getTestLink(userId, productId, insertedLinkId);
		
		//cenário onde ainda não existe um link para esse usuário e produto, logo retorna null
		Mockito.when(linkRepository.findActiveByUserAndProduct(Mockito.eq(userId), Mockito.eq(productId))).thenReturn(null);
		
		//retornando alguma coisa nas chamadas que verificam se existe o produto e o usuário
		Mockito.when(userRepository.findById(Mockito.eq(userId))).thenReturn(Optional.of(new User()));
		Mockito.when(productRepository.findById(Mockito.eq(productId))).thenReturn(Optional.of(new Product()));
		
		Mockito.when(linkRepository.save(Mockito.any())).thenReturn(inserted);
		
		InOrder inOrder = Mockito.inOrder(linkRepository, userRepository, productRepository);
		
		Link result = controller.link(userId, productId).getBody();
		
		Assert.assertEquals(result.getId(), insertedLinkId);
		Assert.assertEquals(result.getUser().getId(), userId);
		Assert.assertEquals(result.getProduct().getId(), productId);
		
		inOrder.verify(linkRepository, Mockito.times(1)).findActiveByUserAndProduct(Mockito.eq(userId), Mockito.eq(productId));
		inOrder.verify(userRepository, Mockito.times(1)).findById(Mockito.eq(userId));
		inOrder.verify(productRepository, Mockito.times(1)).findById(Mockito.eq(productId));
		//chama o save só 1 vez
		inOrder.verify(linkRepository, Mockito.times(1)).save(Mockito.any());
		inOrder.verifyNoMoreInteractions();
	}

	@Test
	public void shouldCreateLinkAndInactivatePrevious() {
		String productId = "444444";
		String userId = "222222";
		String insertedLinkId = "123456789";
		Link inserted = getTestLink(userId, productId, insertedLinkId);
		
		String previousLinkId = "999999999";
		Link previous = getTestLink(userId, productId, previousLinkId);
		
		//cenário onde já existe um link para esse usuário e produto, logo retorna o mesmo
		Mockito.when(linkRepository.findActiveByUserAndProduct(Mockito.eq(userId), Mockito.eq(productId))).thenReturn(previous);
		//como já existe um link que será inativado, vamos chamar save 2 vezes
		Mockito.when(linkRepository.save(Mockito.any())).thenReturn(previous).thenReturn(inserted);
		
		InOrder inOrder = Mockito.inOrder(linkRepository, userRepository, productRepository);
		
		Link result = controller.link(userId, productId).getBody();
		
		Assert.assertEquals(result.getId(), insertedLinkId);
		Assert.assertEquals(result.getUser().getId(), userId);
		Assert.assertEquals(result.getProduct().getId(), productId);
		
		//link que existia antes está marcado como inativo
		Assert.assertFalse(previous.isActive());
		
		inOrder.verify(linkRepository, Mockito.times(1)).findActiveByUserAndProduct(Mockito.eq(userId), Mockito.eq(productId));
		//verifica se realmente chamou 2 vezes
		inOrder.verify(linkRepository, Mockito.times(2)).save(Mockito.any());
		inOrder.verifyNoMoreInteractions();
	}
	
	@Test
	public void shouldNotCreateLinkIfUserIsNotFound() throws Exception {
		String productId = "444444";
		String userId = "222222";
		String insertedLinkId = "123456789";
		Link inserted = getTestLink(userId, productId, insertedLinkId);
		
		//cenário onde ainda não existe um link para esse usuário e produto, logo retorna null
		Mockito.when(linkRepository.findActiveByUserAndProduct(Mockito.eq(userId), Mockito.eq(productId))).thenReturn(null);
		
		//retornando null na validação do usuário
		Mockito.when(userRepository.findById(Mockito.eq(userId))).thenReturn(Optional.empty());
		Mockito.when(productRepository.findById(Mockito.eq(productId))).thenReturn(Optional.of(new Product()));
		
		Mockito.when(linkRepository.save(Mockito.any())).thenReturn(inserted);
		
		InOrder inOrder = Mockito.inOrder(linkRepository, userRepository, productRepository);
		
		ResponseEntity<Link> result = controller.link(userId, productId);
		
		Assert.assertEquals(result.getStatusCode(), HttpStatus.NOT_FOUND);
		
		inOrder.verify(linkRepository, Mockito.times(1)).findActiveByUserAndProduct(Mockito.eq(userId), Mockito.eq(productId));
		inOrder.verify(userRepository, Mockito.times(1)).findById(Mockito.eq(userId));
		inOrder.verify(productRepository, Mockito.times(1)).findById(Mockito.eq(productId));
		inOrder.verifyNoMoreInteractions();
	}
	
	private Link getTestLink(String userId, String productId, String linkId) {
		Link link = new Link();
		link.setId(linkId);
		link.setProduct(new Product(productId));
		link.setUser(new User(userId));
		link.setActive(true);
		return link;
	}

}

