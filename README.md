# Teste para DEV Java

###### Baseado em [Teste para desenvolvedor Java](https://github.com/murilocorreiab/teste-desenvolvedor-java).

## Instruções para rodar o projeto

A partir do diretório raiz do projeto,


Criar imagens no docker:

```
sudo docker build database/ -t mongo-exam && sudo ./mvnw clean package dockerfile:build
```

Executar containers usando docker-compose:

```
sudo docker-compose up
```

## P.S.

Ainda não havia trabalhado com Docker, então tem um potencial para que algo não esteja exatamente no padrão ou até mesmo para que esteja errado. Mas está funcionando.

Qualquer dúvida, entre em contato. Obrigado.

(11) 98821-0364

nando.francisco@gmail.com